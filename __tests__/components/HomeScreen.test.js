import React from 'react';
import { render } from '@testing-library/react-native';
import HomeScreen from '../../src/screens/HomeScreen';

describe('Testing to <HomeScreen />', () => {
  test('Must show the first 40 pokemon cards', () => {
    let component = render(<HomeScreen />);
    expect(component).toBeDefined();
    expect(component.getAllByTestId('pokemon-card').length).toEqual(40);
    expect(component.toJSON()).toMatchSnapshot();
  });

})