import * as React from 'react';
import { View, StyleSheet } from 'react-native';
import SegmentedControlTab from "react-native-segmented-control-tab";

interface Props {
  getTabId: (tabId: number) => void
}

export default function PokemonDetailTabView({ getTabId }: Props) {
  const [index, setIndex] = React.useState(0);

  return (
    <View>
      <SegmentedControlTab 
        values={["About", "Evolution", "Abilities"]}
        borderRadius={0}
        selectedIndex={index}
        onTabPress={(index) => {
          setIndex(index);

          if(typeof getTabId === 'function') {
            getTabId(index);
          }
        }}
        tabStyle={styles.tabStyle}
        activeTabStyle={styles.activeTabStyle}
        tabTextStyle={styles.tabTextStyle}
        activeTabTextStyle={styles.activeTabTextStyle}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  tabsContainerStyle: {
    borderColor: 'transparent'
  },
  tabStyle: {
    borderColor: 'transparent',
    paddingVertical: 15
  },
  tabTextStyle: {
    color: 'black',
    opacity: 0.3
  },
  activeTabStyle: {
    backgroundColor: 'transparent',
    borderBottomColor: 'rgb(125, 136,	223)',
    borderBottomWidth: 2
  },
  activeTabTextStyle: {
    color: 'black',
    fontWeight: 'bold',
    opacity: 1
  }
});


