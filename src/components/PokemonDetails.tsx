import React, { useState } from 'react';
import { StyleSheet, Text, View, ScrollView } from 'react-native';
import { IEvolution, IPokemonDetail } from '../interfaces/IPokemon';
import { FadeInImage } from './FadeInImage';
import PokemonAbilities from './PokemonAbilities';
import PokemonAbout from './PokemonAbout';
import PokemonDetailTabView from './PokemonDetailTabView';
import PokemonEvolution from './PokemonEvolution';

interface Props {
  pokemon: IPokemonDetail;
}

const PokemonDetails = ({ pokemon }: Props) => {
  const [tabContent, settabContent] = useState(0);

  return (
    <View style={styles.container}>
      <View>
        <PokemonDetailTabView 
          getTabId={(index) => settabContent(index) }
        />
      </View>
      <ScrollView
        showsVerticalScrollIndicator={false}
      >
        <View style={styles.tabsCotainer}>
          { tabContent === 0 && (
            <PokemonAbout 
              pokemon={pokemon}
            />
          )}

          { tabContent === 1 && (
            <PokemonEvolution 
              pokemonId={pokemon.id}
            />
          )}

          { tabContent === 2 && (
            <PokemonAbilities
              abilities={pokemon.abilities}
            />
          )}
        </View>
        <View style={{
          marginBottom: 80,
          alignItems: 'center'
        }}>
          <FadeInImage
            uri={pokemon.sprites.front_default}
            style={styles.basicSprites}
          />
        </View>
      </ScrollView>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    marginTop: 40,
    marginBottom: 40,
    padding: 15
  },
  tabsCotainer: {
    paddingVertical: 20
  },
  title: {
    fontSize: 22,
    fontWeight: 'bold',
    marginTop: 20
  },
  basicSprites:  {
    width: 60,
    height: 60
  }
})

export default PokemonDetails
