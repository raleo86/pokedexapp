import React from 'react';
import { View, Text, StyleSheet, ScrollView } from 'react-native';
import { IPokemonDetail } from '../interfaces/IPokemon';
import Progress from './Progress';
import { globalStyles } from '../theme/appTheme';

interface Props {
  pokemon: IPokemonDetail;
}

const PokemonAbout = ({ pokemon }: Props) => {
  return (
    <View>
      <View style={globalStyles.verticalSpacing}>
        <Text style={globalStyles.subtitleContent}>Stats</Text>
        { pokemon.stats.map((stat) => (
          <View 
            key={stat.stat.name}
            style={{
              ...globalStyles.alignHorizontal,
              marginBottom: 5
            }}
          >
            <Text style={styles.statName}>{stat.stat.name}</Text>
            <Text style={{ fontWeight: 'bold' }}>{stat.base_stat}</Text>
            <View style={styles.progressContainer}>
              <Progress color="rgb(125, 136,	223)" width={stat.base_stat} />
            </View>
          </View>
        ))}
      </View>
      <View style={globalStyles.verticalSpacing}>
        <Text style={globalStyles.subtitleContent}>Info</Text>
        <View style={{
          ...globalStyles.alignHorizontal,
          justifyContent: 'space-around'
        }}>
          <View>
            <Text style={styles.infoLabel}>Height</Text>
            <Text>{pokemon.height}</Text>
          </View>
          <View>
            <Text style={styles.infoLabel}>Weight</Text>
            <Text>{pokemon.weight} lbs</Text>
          </View>
          <View>
            <Text style={styles.infoLabel}>Species</Text>
            <Text>{pokemon.species.name}</Text>
          </View>
        </View>
      </View>
      <View style={globalStyles.verticalSpacing}>
        <Text style={globalStyles.subtitleContent}>Base abilities</Text>
        <View style={{
          ...globalStyles.alignHorizontal,
          justifyContent: 'space-around'
        }}>
          <ScrollView 
            showsHorizontalScrollIndicator={false}
            horizontal={true}
          >
          { pokemon.abilities.map((ability) => (
            <View 
              key={ability.ability.name}
              style={{
                ...styles.abilities,
                marginRight: 10
              }}
            >
              <Text style={{ color: 'white' }}>
                {ability.ability.name}
              </Text>
            </View>
          ))}
          </ScrollView>
        </View>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  progressContainer: {
    width: 120
  },
  statName: {
    width: 120,
  },
  infoLabel: {
    color: 'rgba(1, 1, 1, 0.5)',
    marginBottom: 5
  },
  infoWrapper: {
    flexDirection: 'column'
  },
  abilities: {
    backgroundColor: 'rgba(125, 136,	223, 0.8)',
    paddingHorizontal: 10,
    paddingVertical: 5,
    borderRadius: 100
  },
})

export default PokemonAbout
