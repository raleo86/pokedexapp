import React from 'react';
import { StyleSheet, View } from 'react-native';

interface Props {
  color: string,
  width: number
}

const Progress = ({ color, width}: Props) => {
  return (
    <View style={{...styles.outer}}>
      <View style={{
        ...styles.inner, 
        backgroundColor: color, 
        width: width > 100 ? '100%' : `${width}%`
      }}></View>
    </View>
  )
}

const styles = StyleSheet.create({
  outer: {
    width: '100%',
    borderRadius: 100,
    backgroundColor: '#e9e9e9'
  },
  inner: {
    height: 10,
    borderRadius: 100
  }
});

export default Progress
