import React, { useState, useEffect, useRef } from 'react';
import { View, TouchableOpacity, StyleSheet, Text, Dimensions } from 'react-native';
import ImageColors from 'react-native-image-colors';
import { useNavigation } from '@react-navigation/core';
import { ISimplePokemon } from '../interfaces/IPokemon';
import { formatThreeDigits } from '../helpers/utils';
import { FadeInImage } from './FadeInImage';
import { NavigationContainer } from '@react-navigation/native';

interface Props {
  pokemon: ISimplePokemon;
}

export const PokemonCard = ({ pokemon }: Props) => {
  const [bgColor, setbgColor] = useState('grey');
  const isMounted = useRef(true);
  const navigation = useNavigation();

  useEffect(() => {
    ImageColors.getColors(pokemon.picture, { fallback: 'grey' })
      .then(colors => {
        if(!isMounted.current) return;
        
        if(colors.platform === 'android') {
          setbgColor(colors.dominant || 'grey');
        } else {
          setbgColor(colors.background || 'grey');
        }
      });

    return () => {
      isMounted.current = false;
    }
  }, [])

  return (
    <TouchableOpacity
      testID="pokemon-card"
      activeOpacity={0.9}
      onPress={
        () => navigation.navigate('PokemonScreen', { 
          simplePokemon: pokemon,
          color: bgColor
        })
      }
    >
      <View style={{ ...styles.cardContainer, backgroundColor: bgColor }}>
        <FadeInImage 
          uri={pokemon.picture}
          style={styles.image}
        />
        <Text style={styles.name}>
          {pokemon.name}
        </Text>
        <Text style={styles.id}>{'\n#' + formatThreeDigits(pokemon.id)}</Text>
      </View>
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  cardContainer: {
    backgroundColor: 'red',
    height: 100,
    width: Dimensions.get('screen').width * 0.9,
    marginHorizontal: 10,
    marginBottom: 25,
    borderRadius: 10,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.30,
    shadowRadius: 6.65,

    elevation: 8,
  },
  name: {
    color: 'white',
    fontSize: 20,
    fontWeight: 'bold',
    top: 20,
    left: 110,
    textTransform: 'capitalize',
  },
  id: {
    position: 'absolute',
    bottom: -5,
    right: 0,
    fontSize: 38,
    fontWeight: 'bold',
    color: 'white',
    opacity: 0.6
  },
  image: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: 100,
    height: 100
  }
})
