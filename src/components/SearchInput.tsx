import React, { useState, useEffect } from 'react';
import { 
  Platform, 
  StyleProp, 
  StyleSheet, 
  TextInput, 
  View, 
  ViewStyle 
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import useDebounceValue from '../hooks/useDebounceValue';

interface Props {
  onDebounce: (value: string) => void,
  style?: StyleProp<ViewStyle>
}

const SearchInput = ({ style, onDebounce }: Props) => {
  const [textValue, setTextValue] = useState('');
  const debouncedValue = useDebounceValue(textValue);

  useEffect(() => {
    onDebounce(textValue);
  }, [debouncedValue])

  return (
    <View style={{
      ...styles.container,
      ...style as any
    }}>
      <View style={styles.textBackground}>
        <TextInput 
          testID="search-input"
          placeholder="Buscar Pokémon"
          style={{
            ...styles.textInput,
            top: (Platform.OS === 'ios') ? 0 : 2
          }}
          autoCapitalize="none"
          autoCorrect={false}
          value={textValue}
          onChangeText={setTextValue}
        />
        <Icon
          name="search-outline"
          color="grey"
          size={25}
        />
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    // backgroundColor: 'red'
  },
  textBackground: {
    backgroundColor: '#f3f3f3',
    borderRadius: 50,
    height: 40,
    paddingHorizontal: 20,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    shadowColor: "rgba(1, 1, 1, 0.5)",
    shadowOffset: {
      width: 0,
      height: 7,
    },
    shadowOpacity: 0.43,
    shadowRadius: 5.51,
    elevation: 5,
  },
  textInput: {
    flex: 1,
    fontSize: 18
  }
})

export default SearchInput
