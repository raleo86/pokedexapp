import React from 'react';
import { View, Text, ActivityIndicator, StyleSheet, ScrollView } from 'react-native';
import { getPokemonEvolutions, getPokemonPicture } from '../helpers/utils';
import { usePokemonEvolution } from '../hooks/usePokemonEvolution';
import { globalStyles } from '../theme/appTheme';
import { FadeInImage } from './FadeInImage';
import Icon from 'react-native-vector-icons/Ionicons';

interface Props {
  pokemonId: number
}

const PokemonEvolution = ({ pokemonId }: Props) => {
  const { evolution, isLoading } = usePokemonEvolution(pokemonId);

  if(isLoading) {
    return (
      <View style={globalStyles.loadingIndicator}>
        <ActivityIndicator
          size={50}
        />
      </View>
    )

  } else {
    const evolutions = getPokemonEvolutions(evolution.chain);

    return (
      <View>
        <Text style={globalStyles.subtitleContent}>Evolution Chain</Text>
        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
          <View 
            style={{
              ...globalStyles.alignHorizontal,
              marginVertical: 30,
              paddingHorizontal: 15
            }}
          >
            { evolutions.map((evolution, index) => (
              <View style={styles.container} key={evolution.id}>
                <View style={styles.evolution}>
                  <FadeInImage
                    uri={getPokemonPicture(evolution.id)}
                    style={styles.evolutionImg}
                  />
                  <Text style={styles.evolutionName}>
                    {evolution.name}
                  </Text>
                </View>
                { (index < (evolutions.length - 1)) && (
                  <Icon
                    style={{ marginHorizontal: 10 }}
                    name="chevron-forward-outline" 
                    color="rgb(125, 136,	223)" 
                    size={50} 
                  />
                )}
              </View>
            ))}
          </View>
        </ScrollView>
      </View>
    )
  }

}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center'
  },
  evolution: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  evolutionImg: {
    width: 100,
    height: 100
  },
  evolutionName: {
    fontSize: 16,
    textTransform: 'capitalize'
  }
})

export default PokemonEvolution
