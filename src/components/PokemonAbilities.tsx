import React, { useEffect, useState } from 'react';
import { View, Text } from 'react-native';
import { pokemonApi } from '../api/pokemonApi';
import { Ability, IAbility } from '../interfaces/IPokemon';
import { globalStyles } from '../theme/appTheme';

interface Props {
  abilities: Ability[]
}

const PokemonAbilities = ({ abilities }: Props) => {
  const [abilitiesDescription, setabilitiesDescription] = useState<IAbility[]>([]);

  useEffect(() => {
    getAbilitiesDescription();
  }, []);

  const fetcher = async(path: string) => {
    let response = await pokemonApi.get<IAbility>(path);
    return response.data;
  }

  const getAbilitiesDescription = async () => {
    let fetchers: Promise<any>[] = [];

    abilities.map((ability) => {
      fetchers.push(fetcher(ability.ability.url));
    });

    const response: IAbility[] = await Promise.all(fetchers);
    setabilitiesDescription(response);
  }

  return (
    <View>
      {abilitiesDescription.map(ability => {
        const effectEntry = ability.effect_entries.find(effect => effect.language.name === 'en');

        return(
          <View key={ability.id} style={{ marginBottom: 30 }}>
            <Text 
              style={{...globalStyles.subtitleContent, textTransform: 'capitalize'
            }}>
              {ability.name}
            </Text>
            <Text 
              style={{
                color: 'rgba(1, 1, 1, 0.6)',
                marginBottom: 15
              }}
            >
              {effectEntry?.short_effect}
            </Text>
            <Text>{effectEntry?.effect}</Text>
          </View>
        )
      })}
    </View>
  )
}

export default PokemonAbilities
