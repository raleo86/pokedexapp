import React from 'react';
import { Platform } from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Navigator } from './Navigator';
import { TabSearchScreen } from './TabSearch';
import Icon from 'react-native-vector-icons/Ionicons';

const Tab = createBottomTabNavigator();

const Tabs = () => {
  return (
    <Tab.Navigator 
      sceneContainerStyle={{
        backgroundColor: 'white'
      }}
      screenOptions={{
        headerShown: false,
        tabBarActiveTintColor: 'rgb(125, 136,	223)',
        tabBarLabelStyle: {
          marginBottom: (Platform.OS === 'ios' ? 0 : 10)
        },
        tabBarStyle: {
          position: 'absolute',
          backgroundColor: 'rgba(255, 255, 255, 0.9)',
          borderWidth: 0,
          elevation: 0,
          height: (Platform.OS === 'ios' ? 80 : 60)
        }
      }}
    >
      <Tab.Screen 
        name="HomeScreen" 
        component={Navigator} 
        options={{
          tabBarLabel: 'Listado',
          tabBarIcon: ({ color }) => (
            <Icon 
              color={color} 
              size={25} 
              name="list-outline"
            />
          )
        }}
      />
      <Tab.Screen 
        name="SearchScreen" 
        component={TabSearchScreen}
        options={{
          tabBarLabel: 'Buscar',
          tabBarIcon: ({ color }) => (
            <Icon 
              color={color} 
              size={25} 
              name="search-outline"
            />
          )
        }} 
      />
    </Tab.Navigator>
  );
}

export default Tabs