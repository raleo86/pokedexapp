import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import HomeScreen from '../screens/HomeScreen';
import PokemonScreen from '../screens/PokemonScreen';
import { ISimplePokemon } from '../interfaces/IPokemon';

export type RootStackParamas = {
  PokemonListScreen: undefined,
  PokemonScreen: {
    simplePokemon: ISimplePokemon,
    color: string
  }
}

const Stack = createStackNavigator<RootStackParamas>();

export const Navigator= () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
        cardStyle: {
          backgroundColor: 'white'
        }
      }}
    >
      <Stack.Screen name="PokemonListScreen" component={HomeScreen} />
      <Stack.Screen name="PokemonScreen" component={PokemonScreen} />
    </Stack.Navigator>
  );
}