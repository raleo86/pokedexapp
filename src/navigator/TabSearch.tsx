
import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { RootStackParamas } from './Navigator';
import SearchScreen from '../screens/SearchScreen';
import PokemonScreen from '../screens/PokemonScreen';

const TabSearch = createStackNavigator<RootStackParamas>();

export const TabSearchScreen = () => {
  return (
    <TabSearch.Navigator
      screenOptions={{
        headerShown: false,
        cardStyle: {
          backgroundColor: 'white'
        }
      }}
    >
      <TabSearch.Screen name="PokemonListScreen" component={SearchScreen} />
      <TabSearch.Screen name="PokemonScreen" component={PokemonScreen} />
    </TabSearch.Navigator>
  );
}