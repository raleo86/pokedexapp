import { IPokemonEvolution, IPokemonEvolutionResult } from "../interfaces/IHelpers";
import { Chain } from "../interfaces/IPokemon";

export const formatThreeDigits = (id: string | number): string => {
  let identifier = typeof id === 'string' ? id : id.toString();
  const idLength = identifier.length;

  if(idLength == 1)
    return `00${identifier}`;
  if(idLength == 2)
    return `0${identifier}`;
  
  return identifier.toString();
}

export const getPokemonPicture = (id: number): string => {
  return `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/${id}.png`
}

export const getPokemonEvolutions = (
  data: Chain, result: IPokemonEvolutionResult[] | any = []
): IPokemonEvolutionResult[] => {
  let pokemon: IPokemonEvolutionResult[] = [];
  pokemon = [...result, {
    id: data.species.url.split('/pokemon-species/')[1].replace('/', ''),
    name: data.species.name
  }];
  if(data.evolves_to.length > 0) {
    return  getPokemonEvolutions(data.evolves_to[0], pokemon)
  } else {
    return pokemon;
  }
}