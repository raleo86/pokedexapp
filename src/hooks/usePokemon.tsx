
import { useState, useEffect } from 'react';
import { IEvolution, IPokemonDetail } from "../interfaces/IPokemon";
import { pokemonApi } from "../api/pokemonApi";

export const usePokemon = (id: string) => {
  const [isLoading, setIsLoading] = useState(true);
  const [pokemon, setPokemon] = useState<IPokemonDetail>({} as IPokemonDetail);
  const [evolution, setEvolution] = useState<IEvolution>({} as IEvolution);

  useEffect(() => {
    getPokemon();
  }, [])

  const getPokemon = async() => {
    setIsLoading(true);
    const resDetail = await pokemonApi.get<IPokemonDetail>(`https://pokeapi.co/api/v2/pokemon/${id}`);
    setPokemon(resDetail.data);
    setIsLoading(false);
  }

  return {
    isLoading,
    pokemon,
    evolution
  }
}