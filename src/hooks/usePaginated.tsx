import { useEffect, useRef, useState } from 'react'
import { pokemonApi } from '../api/pokemonApi';
import { IPokemon, IResult, ISimplePokemon } from '../interfaces/IPokemon';
import { getPokemonPicture } from '../helpers/utils';

export const usePaginated = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [pokemonList, setPokemonList] = useState<ISimplePokemon[]>([]);
  const nextPageUrl = useRef('https://pokeapi.co/api/v2/pokemon?limit=40');

  useEffect(() => {
    getPokemons();
  }, []);

  const getPokemons = async() => {
    setIsLoading(true);
    const response = await pokemonApi.get<IPokemon>(nextPageUrl.current);
    nextPageUrl.current = response.data.next;
    mapPokemonList(response.data.results);
  }

  const mapPokemonList = (list: IResult[]) => {
    const newPokemonList: ISimplePokemon[] = list.map(({name, url}) => {
      const urlParts = url.split('/');
      const id = urlParts[urlParts.length - 2];
      const picture = getPokemonPicture(parseInt(id));

      return { id, picture, name }
    });

    setPokemonList([...pokemonList, ...newPokemonList]);
    setIsLoading(false);
  }

  return {
    isLoading,
    pokemonList,
    loadPokemons: getPokemons
  }
}
