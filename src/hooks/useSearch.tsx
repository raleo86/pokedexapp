import { useEffect, useState } from 'react'
import { pokemonApi } from '../api/pokemonApi';
import { IPokemon, IResult, ISimplePokemon } from '../interfaces/IPokemon';
import { getPokemonPicture } from '../helpers/utils';

export const useSearch = () => {
  const [isFetching, setIsFetching] = useState(true);
  const [pokemonList, setPokemonList] = useState<ISimplePokemon[]>([]);

  useEffect(() => {
    getPokemons();
  }, []);

  const getPokemons = async() => {
    const response = await pokemonApi.get<IPokemon>('https://pokeapi.co/api/v2/pokemon?limit=1200');
    mapPokemonList(response.data.results);
  }

  const mapPokemonList = (list: IResult[]) => {
    const newPokemonList: ISimplePokemon[] = list.map(({name, url}) => {
      const urlParts = url.split('/');
      const id = urlParts[urlParts.length - 2];
      const picture = getPokemonPicture(parseInt(id));

      return { id, picture, name }
    });

    setPokemonList(newPokemonList);
    setIsFetching(false);
  }

  return {
    isFetching,
    pokemonList
  }
}
