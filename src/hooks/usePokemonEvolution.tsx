
import { useState, useEffect } from 'react';
import { IEvolution, ISpecies } from "../interfaces/IPokemon";
import { pokemonApi } from "../api/pokemonApi";

export const usePokemonEvolution = (id: number) => {
  const [isLoading, setIsLoading] = useState(true);
  const [evolution, setEvolution] = useState<IEvolution>({} as IEvolution);

  useEffect(() => {
    getPokemonEvolutions();
  }, [])

  const getPokemonEvolutions = async() => {
    setIsLoading(true);
    const resSpecies = await pokemonApi.get<ISpecies>(`https://pokeapi.co/api/v2/pokemon-species/${id}`);
    const resEvolutions = await pokemonApi.get<IEvolution>(resSpecies.data.evolution_chain.url);
    setEvolution(resEvolutions.data);
    setIsLoading(false);
  }

  return {
    isLoading,
    evolution
  }
}