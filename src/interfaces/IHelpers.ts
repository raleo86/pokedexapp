export interface IPokemonEvolution {
  chains?: IPokemonEvolutionResult[];
}

export interface IPokemonEvolutionResult {
  id:   number;
  name: string;
}