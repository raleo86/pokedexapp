import React from 'react';
import { 
  FlatList, 
  Image, 
  ActivityIndicator, 
  Text, 
  View 
} from 'react-native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { PokemonCard } from '../components/PokemonCard';
import { usePaginated } from '../hooks/usePaginated';
import { globalStyles } from '../theme/appTheme';

const HomeScreen = () => {
  const { top } = useSafeAreaInsets();
  const { pokemonList, loadPokemons } = usePaginated();

  return (
    <>
      <Image 
        source={require('../assets/pokebola.png')} 
        style={globalStyles.pokeball}
      />
      <View style={{ flex: 1, marginHorizontal: 10 }}>
        <FlatList
          data={pokemonList}
          keyExtractor={(pokemon) => pokemon.id}
          renderItem={({ item }) => (<PokemonCard pokemon={item} />)}
          ListHeaderComponent={(
            <Text
              style={{
                ...globalStyles.title,
                ...globalStyles.globalMargin,
                top: top + 20,
                marginBottom: top + 40
              }}
            >
              Pokedex
            </Text>
          )}
          onEndReached={loadPokemons}
          onEndReachedThreshold={0.40}
          ListFooterComponent={<ActivityIndicator style={{ height: 100 }} size={20} color="grey" />}
        />
      </View>
    </>
  )
}

export default HomeScreen
