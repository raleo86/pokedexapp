import React, { useState, useEffect } from 'react';
import { View, ActivityIndicator, Text, FlatList, Platform } from 'react-native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { PokemonCard } from '../components/PokemonCard';
import SearchInput from '../components/SearchInput';
import { useSearch } from '../hooks/useSearch';
import { ISimplePokemon } from '../interfaces/IPokemon';
import { globalStyles } from '../theme/appTheme';


const SearchScreen = () => {
  const { top } = useSafeAreaInsets();
  const { isFetching, pokemonList } = useSearch();
  const [term, setTerm] = useState('');
  const [pokemonFiltered, setpokemonFiltered] = useState<ISimplePokemon[]>([]);

  useEffect(() => {
    if(term.length === 0) {
      return setpokemonFiltered([]);
    }

    if(isNaN(Number(term))) {
      setpokemonFiltered(
        pokemonList.filter(
          poke => poke.name.toLocaleLowerCase()
            .includes(term.toLowerCase())
        )
      );
    } else {
      const pokemonById = pokemonList.find(poke => poke.id === term);
      setpokemonFiltered(
        (pokemonById) ? [pokemonById] : []
      );
    }
    
    
  }, [term])

  if(isFetching) {
    return (
      <View style={globalStyles.loadingIndicator}>
        <ActivityIndicator
          size={50}
          color="rgb(125, 136,	223)"
        />
        <Text style={{color: "rgb(125, 136,	223)"}}>Cargando...</Text>
      </View>
    )
  }

  return (
    <View style={{ 
      flex: 1, 
      top: top + 20,
      marginHorizontal: 10
    }}>
      <SearchInput 
        onDebounce={setTerm}
      />
      <View style={{ flex: 1 }}>
        <FlatList
            data={pokemonFiltered}
            keyExtractor={(pokemon) => pokemon.id}
            showsVerticalScrollIndicator={false}
            renderItem={({ item }) => (<PokemonCard pokemon={item} />)}
            ListHeaderComponent={(
              <Text
                style={{
                  ...globalStyles.title,
                  top: 20,
                  marginBottom: Platform.OS === 'ios' ? top : top + 40
                }}
              >
                {term}
              </Text>
            )}
          />
      </View>
    </View>
  )
}

export default SearchScreen;
