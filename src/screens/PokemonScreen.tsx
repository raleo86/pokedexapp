import React from 'react';
import { 
  View, 
  Text, 
  StyleSheet, 
  TouchableOpacity, 
  Dimensions, 
  ActivityIndicator, 
  Platform
} from 'react-native';
import { StackScreenProps } from '@react-navigation/stack';
import { RootStackParamas } from '../navigator/Navigator';
import Icon from 'react-native-vector-icons/Ionicons';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { formatThreeDigits } from '../helpers/utils';
import { usePokemon } from '../hooks/usePokemon';
import { FadeInImage } from '../components/FadeInImage';
import PokemonDetails from '../components/PokemonDetails';
import { globalStyles } from '../theme/appTheme';

interface Props extends StackScreenProps<RootStackParamas, 'PokemonScreen'> {};

const PokemonScreen = ({navigation, route}: Props) => {
  const { simplePokemon, color } = route.params;
  const { top } = useSafeAreaInsets();

  const { isLoading, pokemon, evolution } = usePokemon(simplePokemon.id);

  return (
    <View style={{
      ...styles.container,
      backgroundColor: color 
    }}>
      <View style={{
        ...styles.header,
      }}>
        <TouchableOpacity
          onPress={() => navigation.pop()}
          activeOpacity={0.8}
          style={{
            ...styles.backButton,
            top: top + 10
          }}
        >
          <Icon name="arrow-back-outline" color="white" size={30} />
        </TouchableOpacity>
        <View style={{
          ...styles.alignElements,
          ...globalStyles.alignHorizontal,
          top: top + 60
        }}>
          <View>
            <Text style={{...styles.name}}>
              {simplePokemon.name}
            </Text>
          </View>
          <View>
            <Text style={{...styles.id}}>#{formatThreeDigits(simplePokemon.id)}</Text>
          </View>
        </View>
        <View style={{...styles.alignElements, top: top + 65}}>
          <View style={{ flexDirection: 'row'}}>
            { Object.keys(pokemon).length > 0 && pokemon.types.map(({ type }) => (
              <View key={type.name} style={styles.types}>
                <Text 
                  style={styles.typesText}
                  key={type.name}
                >
                  {type.name}
                </Text>
              </View>
            ))}
          </View>
        </View>
      </View>
      <View style={{
        ...styles.body
      }}>
        <FadeInImage
          uri={simplePokemon.picture}
          style={styles.image}
        />
        { isLoading ? (
          <View style={{...globalStyles.loadingIndicator}}>
            <ActivityIndicator 
              color={color}
              size={50}
            />
          </View>
        ) : (
          <PokemonDetails pokemon={pokemon} />
        )}
        
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  header: {
    height: Platform.OS === 'ios' ? 370 : 300,
    width: Dimensions.get('screen').width,
    zIndex: 1,
    position: 'relative'
  },
  body: {
    flex: 1,
    backgroundColor: 'white',
    borderTopStartRadius: 30,
    borderTopEndRadius: 30,
    zIndex: 2
  },
  backButton: {
    position: 'absolute',
    left: 20
  },
  name: {
    color: 'white',
    fontSize: 28,
    fontWeight: 'bold',
    textTransform: 'capitalize',
  },
  image: {
    width: 200,
    height: 200,
    position: 'absolute',
    left: Dimensions.get('screen').width * 0.12,
    top: -80
  },
  id: {
    color: 'white',
    fontSize: 19,
    fontWeight: 'bold',
  },
  alignElements: {
    paddingHorizontal: 20,
  },
  types: {
    borderRadius: 100,
    backgroundColor: 'rgba(1, 1, 1, 0.3)',
    marginRight: 10,
    marginTop: 10,
    paddingHorizontal: 15,
    paddingVertical: 5
  },
  typesText: {
    color: 'white',
    textTransform: 'capitalize'
  }
})

export default PokemonScreen;
