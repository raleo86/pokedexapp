import { StyleSheet } from 'react-native';

export const globalStyles = StyleSheet.create({
  globalMargin: {
    marginHorizontal: 20
  },
  pokeball: {
    position: 'absolute',
    width: 300,
    height: 300,
    top: -100,
    right: -100,
    opacity: 0.2
  },
  title: {
    fontSize: 35,
    fontWeight: 'bold'
  },
  subtitleContent: {
    fontSize: 18,
    fontWeight: 'bold',
    marginBottom: 15
  },
  alignHorizontal: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  verticalSpacing: {
    marginVertical: 20
  },
  loadingIndicator: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
})